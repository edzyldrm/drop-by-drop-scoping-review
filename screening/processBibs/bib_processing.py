import bibtexparser
import re
from Levenshtein import distance
import random
import copy


'''
Updated bibtex modifier for bibtexparser v1.4.0, write to file enabled.
'''


def clean_text(text):
        text_no_special_chars = re.sub(r'[^a-zA-Z0-9\s]', '', text)
        text_no_spaces = text_no_special_chars.replace(" ", "")
        return text_no_spaces.lower()

def calculate_levenshtein_distance(library, p=False):
    '''
    import bibtexparser
    import re
    from Levenshtein import distance
    Updated for newest bibtexparser that contains 'write to file' method.
    For every title pair, checks the levenshtein distance. If the distance is smaller than 10% of the mean of 2 titles, marks as duplicate and 
    writes the duplicate indices to "scr_tit_dup" field and duplicate qurids to 'scr_tit_dup_q' field.

    Args: 
        library: bibtex lib object
        p: print similarities as found
    ''' 
    
    similar_titles = {}
    for index1, entry1 in library.entries_dict.items():
        q1 = entry1['qurid']
        dlist = [index1]
        qlist = [q1]
        title1 = entry1['title']
        #Create an empty 'scr_tit_dup_q' field 
        entry1['scr_tit_dup'] = ''
        cleaned_title1= clean_text(title1)
        t1short = str(cleaned_title1)[:20]
        #print(f'checking for {t1short}')
        for index2, entry2 in library.entries_dict.items():
            if index1 != index2:
                title2 = entry2['title']
                q2 = entry2['qurid']
                cleaned_title2 = clean_text(title2)
                t2_short = str(cleaned_title2)[:20]
                mean_length = (len(cleaned_title1) + len(cleaned_title2)) / 2
                dist = distance(cleaned_title1, cleaned_title2)
                if dist < 0.01 * mean_length: 
                    if p == 'full':
                        print(f'found similarity between {t1short} _index_{index1}_and_ {t2_short} _index_{index2} _dist {dist}')     
                    dlist.append(index2)
                    qlist.append(q2)
                    entry1['scr_tit_dup'] = ', '.join(dlist)
                    entry1['scr_tit_dup_q'] = ', '.join(qlist) #updated for qurid

                    if cleaned_title1 not in similar_titles:
                        indList = []
                        indList.append(index1)
                        indList.append(index2)
                        similar_titles[cleaned_title1] = indList
                    else:
                        similar_titles[cleaned_title1].append(str(index2))
    for i,j in similar_titles.items():
        similar_titles[i] = [x for n, x in enumerate(similar_titles[i]) if x not in similar_titles[i][:n]]
    return library, similar_titles


def check_keywords(library, p=False):
    '''
    checks if any of the keywords exists anywhere in the title or abstract. Converts the titles, abstracts, and the keywords to lowercase and removes special characters before checking. 
    Updates 'scr_kw' field with found keyword(first) and where it is found (title or abstract)
    '''
    keywords = [
        "Dietary", 
        "Diabet", 
        "Sedentary", 
        "Food", 
        "Game changer",
        "Energy drinks", 
        "Sports", 
        "Overweight", 
        "Supply chain",
        "Biomass", 
        "Route", 
        "WiFi", 
        "WSN", 
        "Wireless Sensor Networks",
        "Cardio",
        "VoIP",
        "Acoustic",
        "Cybersecurity",
        "PAH",
        "Battery",
        "Game theory",
        "Game-theory",
        "Game theoretic",
        "Game-theoretic",
        "Games theory",
        "Game theoretical",
        "Game-theoretical",
        "Game change",
        "Game-change",
        "Game changer",
        "Gamechanger",
        "Gamechanging",
        "Game-changing",
        "Game-changer"
        ]
    report = {}
    for index, entry in library.entries_dict.items():
        entry['scr_kw'] = ''
        title_exists = 'title' in entry
        abstract_exists = 'abstract' in entry
        title = entry['title'].lower() if title_exists else ""
        abstract = entry['abstract'].lower() if abstract_exists else ""
        for keyword in keywords:
            keyword_clean = clean_text(keyword)
            found = False
            if keyword_clean in clean_text(title):
                location = 'title'
                found = True
            elif keyword_clean in clean_text(abstract):
                location = 'abstract'
                found = True
            if found:
                formatted_string = f"Exclude, found \'{keyword}\' in {location}"
                entry['scr_kw'] = formatted_string
                report[index] = formatted_string
                break
    return library, report


def check_dois(library, p=False):
    """
    Update the 'scr_doi_dup' field with a list of duplicate items' IDs.
    Update the 'scr_doi_dup_q' field with a list of duplicate items' QURIDs.
    Args:
        library: A BibTeX library object.
        p: Boolean flag to enable printing debug information. Default is False.
    
    Returns:
        Modified library object with the 'doi_duplicate' and 'scr_doi_dup_q' fields updated.
    """

    doi_duplicates = {}
    doi_duplicates_q = {} #qurid

    for index, entry in library.entries_dict.items():
        doi = str(entry['doi']).replace(' || NA', '').strip()
        qurid = entry['qurid']
        if doi != 'NA':
            if doi not in doi_duplicates:
                doi_duplicates[doi] = [index]
                doi_duplicates_q[doi]  = [qurid]
            else:
                doi_duplicates[doi].append(index)
                doi_duplicates_q[doi].append(qurid)

    for index, entry in library.entries_dict.items():
        doi = str(entry['doi']).replace(' || NA', '').strip()
        qurid = entry['qurid']
        if doi != 'NA':
            entry['scr_doi_dup'] = ', '.join(str(i) for i in doi_duplicates[doi])
            entry['scr_doi_dup_q'] = ', '.join(str(i) for i in doi_duplicates_q[doi])
        else:
            entry['scr_doi_dup'] = 'NA' #changed to NA instead of empty
            entry['scr_doi_dup_q'] = 'NA'
    report = {doi: (len(entries), entries) for doi, entries in doi_duplicates.items() if len(entries) > 1}

    return library, report


def mark_primary_entry(library, p=False):
    """
    Mark a primary entry among duplicates and update the 'scr_is_tit_dup_of' and 'scr_is_doi_dup_of' fields. The entry with the smallest ID in duplications is the primary one.    
    Args:
        library: A BibTeX library object.

    Returns:
        The modified library object with marked primary entries and updated 'scr_is_tit_dup_of' and 'scr_is_doi_dup_of' fields.
    """
    for index, entry in library.entries_dict.items():
        # Update 'scr_is_tit_dup_of' field
        title_duplicate = entry['scr_tit_dup'] if 'scr_tit_dup' in entry else ''
        entry['scr_is_tit_dup_of'] = ''
        if title_duplicate:
            duplicate_ids = [int(id.strip()) for id in title_duplicate.split(',')]
            primary_id = min(duplicate_ids)
            primary_entry = library.entries_dict[str(primary_id)]
            primary_qurid = primary_entry['qurid'] if 'qurid' in primary_entry else ''
            primary_entry['scr_is_tit_dup_of'] = f'primary, {primary_qurid}'
            for duplicate_id in duplicate_ids:
                if duplicate_id != primary_id:
                    duplicate_entry = library.entries_dict[str(duplicate_id)]
                    duplicate_entry['scr_is_tit_dup_of'] = primary_qurid
        
        # Update 'scr_is_doi_dup_of' field
        doi_duplicate = entry['scr_doi_dup'] if 'scr_doi_dup' in entry else ''
        entry['scr_is_doi_dup_of'] = ''
        if doi_duplicate:
            if doi_duplicate != 'NA':
                duplicate_ids = [int(id.strip()) for id in doi_duplicate.split(',')]
                primary_id = min(duplicate_ids)
                primary_entry = library.entries_dict[str(primary_id)]
                primary_qurid = primary_entry['qurid'] if 'qurid' in primary_entry else ''
                primary_entry['scr_is_doi_dup_of'] = f'primary, {primary_qurid}'
                for duplicate_id in duplicate_ids:
                    if duplicate_id != primary_id:
                        duplicate_entry = library.entries_dict[str(duplicate_id)]
                        duplicate_entry['scr_is_doi_dup_of'] = primary_qurid
    
    return library

def mark_items_to_remove(library, p=False):
    """
    Mark items to be removed based on their 'scr_is_tit_dup_of', 'scr_is_doi_dup_of', and 'scr_kw' fields.
    In case of duplication, the entry with the smalles ID (primary) is kept.
    update 'scr_dec'
    Args:
        library: A BibTeX library object.

    Returns:
        The modified library object with marked items to be removed in the 'scr_dec' field.
    """
    ectr, tctr, dctr, kctr, ctr = 0, 0, 0, 0, 0
    for index, entry in library.entries_dict.items():
        is_title_duplicate_of = entry.get('scr_is_tit_dup_of', '')
        is_doi_duplicate_of = entry.get('scr_is_doi_dup_of', '')
        excluded = entry.get('scr_kw', '')

        if is_title_duplicate_of.startswith('qurid') or is_doi_duplicate_of.startswith('qurid') or 'exclude' in excluded.lower():
            entry['scr_dec'] = 'exclude'
            ctr += 1

            if 'exclude' in excluded.lower():
                entry['scr_dec'] += '_keyword'
                ectr += 1

            if is_title_duplicate_of.startswith('qurid'):
                entry['scr_dec'] += '_titledup'
                tctr += 1
            if is_doi_duplicate_of.startswith('qurid'):
                entry['scr_dec'] += '_doidup'
                dctr += 1
        elif is_title_duplicate_of.startswith('primary') or is_doi_duplicate_of.startswith('primary'):
            entry['scr_dec'] = 'keep'
            kctr += 1
        else:
            entry['scr_dec'] = 'keep'
            kctr += 1
    if p:
        print(f'{len(library.entries_dict)} items')
        print(f'{ectr} items were excluded with keywords')
        print(f'{dctr} items were excluded with dois')
        print(f'{tctr} items were excluded with titles')
        print(f'{ctr} items were excluded in total')
        print(f'{kctr} items were marked to keep')
    
    return library



def split_bibtex(bibtex_object, write=False, prefix="split", p=False):
    try:
        total_entries = len(bibtex_object.entries)
        all_indices = list(range(total_entries))
        random.shuffle(all_indices)
        split_index = total_entries // 2
        group1_indices = all_indices[:split_index]
        group2_indices = all_indices[split_index:]

        bibtex_group1 = copy.deepcopy(bibtex_object)
        bibtex_group2 = copy.deepcopy(bibtex_object)

        bibtex_group1.entries = [bibtex_object.entries[i] for i in group1_indices]
        bibtex_group2.entries = [bibtex_object.entries[i] for i in group2_indices]

        if write:
            filename1 = f"{prefix}_1.bib"
            filename2 = f"{prefix}_2.bib"
            print(f'files {filename1} and {filename2} were created')
            with open(filename1, 'w') as bibtex_file1:
                bibtexparser.dump(bibtex_group1, bibtex_file1)
            with open(filename2, 'w') as bibtex_file2:
                bibtexparser.dump(bibtex_group2, bibtex_file2)

        return bibtex_group1, bibtex_group2

    except Exception as e:
        print(f"Error occurred while splitting: {e}")
        return None, None
    

def processLib(library, write = False, filename=None, p=False):
    '''runs the processing functions sequentially, returns 1 library object and 3 reports. 
    
    1st report: duplicate DOIs
    2nd report: keyword exclusion per index
    3rd report: duplicate titles

    added fields to the lib object:
        scr_doi_dup : a list (as string) of duplicate DOIs
        scr_kw: reason for exclusion (Exclude, Keyword X found in Y)
        scr_tit_dup: indices of duplicate titles (str)
        scr_tit_dup_q: qurid id of duplicate titles (str)
        scr_is_tit_dup_of: list of qurids of duplicates. if the item is primary, then it is 'primary_{qurid}'
        scr_is_doi_dup_of: list of qurids of duplicates. if the item is primary, then it is 'primary_{qurid}'
        scr_dec: the final decision to keep or to exclude, with reason(s). 
        

    '''
    if write:
        if filename is None:
            raise ValueError("Filename argument is required when w is True")
    
    print(f'Checking Keywords for Exclusion')
    excLib, excRep = check_keywords(library, p=p)
    print(f'Complete')

    print(f'Checking DOIs for duplicates')
    doiLib, doiRep = check_dois(excLib, p=p)
    print(f'Complete')

    print(f'Checking for Duplicates in Title')
    dupLib, dupRep = calculate_levenshtein_distance(doiLib, p=p)

    print(f'Marking primary entries')
    plib = mark_primary_entry(dupLib, p=p)
    
    print(f'Updating Final Decision')
    mlib = mark_items_to_remove(plib, p=p)

    if write:
        with open(filename, 'w') as bibtex_file: # type: ignore
                bibtexparser.dump(mlib, bibtex_file)
        print(f'file write complete {filename}')
    return mlib, doiRep, excRep, dupRep



from itertools import combinations

def compare_entries(library, duprep):
    """
    Compare entries within each group of duplicates and record differences in specified fields and author surnames.
    
    Args:
        library: A BibTeX library object containing the entries to compare.
        duprep: A dictionary where keys are identifiers for groups of duplicates and values are lists of indices 
                representing entries within each group.
        
    Returns:
        A dictionary containing information about the differences found between entries within each group of duplicates.
    """
    fields_to_compare = ['year', 'volume', 'publisher', 'issn', 'doi']
    dbcheck = {}

    for group_id, group_indices in duprep.items():
        author_surnames = [clean_text(library.entries_dict[index]['author'].split(',', 1)[0].strip()) for index in group_indices]
        
        for pair in combinations(group_indices, 2):
            index1, index2 = pair
            author_surname1 = author_surnames[group_indices.index(index1)]
            author_surname2 = author_surnames[group_indices.index(index2)]

            for field in fields_to_compare:
                value1 = library.entries_dict[index1][field]
                value2 = library.entries_dict[index2][field]

                if value1 != 'NA' and value2 != 'NA':
                    if value1 != value2:
                        difference = f'unidentical {field} between i{index1} and i{index2}: {value1} vs {value2}'
                        dbcheck.setdefault(group_id, []).append(difference)

            if author_surname1 != author_surname2:
                difference = f'unidentical author surname between i{index1} and i{index2}: {author_surname1} vs {author_surname2}'
                dbcheck.setdefault(group_id, []).append(difference)

    return dbcheck