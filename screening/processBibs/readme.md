﻿﻿# BibTeX Processing Tool

This tool provides functions for processing BibTeX files to check for duplicate DOIs, exclude entries based on keywords, and identify duplicate titles.

## Installation

1. Clone this repository to your local machine.
2. Install the required dependencies using pip:

```bash
pip install bibtexparser python-Levenshtein
```

## Usage
### A func to sequentially process a library object, update corresponding fields, create reports, and write to a new file (optional) 

#### `processLib(library, write=False, filename=None, p=False)`
- Description: Runs the processing functions sequentially and returns the modified library and reports.
- Arguments:
  - `library`: A BibTeX library object.
  - `write` (optional): Boolean flag to enable writing the modified library to a file. Default is `False`.
  - `filename` (optional): Name of the output file to write the modified library. Required if `write` is `True`.
  - `p` (optional): Boolean flag to enable printing debug information. Default is `False`.
- Returns: Modified BibTeX library object, report of duplicate DOIs, report of excluded entries, and report of similar titles.

```python
import bibtexparser

# Parse the BibTeX file
with open('library.bib', 'r') as bibtex_file:
    library = bibtexparser.load(bibtex_file)

# Process the library
processed_library, doi_report, exclusion_report, title_report = processLib(library, write=True, filename='processed_library.bib', p=True)
```

### Functions

#### `check_dois(library, p=False)`
- Description: Checks for duplicate DOIs in the BibTeX library and updates entries with duplicate DOIs. Now includes handling of QURIDs.
- Arguments:
  - `library`: A BibTeX library object.
  - `p` (optional): Boolean flag to enable printing debug information. Default is `False`.
- Returns: Modified BibTeX library object and a report of duplicate DOIs.

#### `check_keywords(library, p=False)`
- Description: Checks for keywords in the titles and abstracts of entries to exclude them. Updates `scr_kw` field with found keywords.
- Arguments:
  - `library`: A BibTeX library object.
  - `p` (optional): Boolean flag to enable printing debug information. Default is `False`.
- Returns: Modified BibTeX library object and a report of excluded entries.

#### `calculate_levenshtein_distance(library, p=False)`
- Description: Calculates the Levenshtein distance between titles in the BibTeX library and marks similar titles. Now updates `scr_tit_dup` and `scr_tit_dup_q` fields.
- Arguments:
  - `library`: A BibTeX library object.
  - `p` (optional): Boolean flag to enable printing debug information. Default is `False`.
- Returns: Modified BibTeX library object and a report of similar titles.

#### `split_bibtex(library, write=True, prefix= "prefix")`
- Splits bib object into 2 randomly split files. If write=True, it also writes to 2 different files using the prefix_1.bib and prefix_2.bib as file names.
- Example usage:
```python
bibtex_group1, bibtex_group2 = split_bibtex(library, write=True, prefix="final_splitfile")
```

#### Note:
- bibtexparser can't parse if some special characters exist in some (most) fields ['{', '}'] and marks them as 'comment' entries. We manually fixed the issues in the bib file before processing the libraries.
