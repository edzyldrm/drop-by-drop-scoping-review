# PsychINFO (through the EbscoHost interface)

((TI ("energy saving" OR "energy conservation")) OR (AB ("energy saving" OR "energy conservation"))) AND (((TI gami*) OR (AB gami*)) OR ((TI ("intervention" OR "behavior change" OR "behaviour change")) OR (AB ("intervention" OR "behavior change" OR "behaviour change"))) OR ((TI ("machine learning" OR "artificial intelligence")) OR (AB ("machine learning" OR "artificial intelligence"))))

# ACM (through the ACM Digital Library interface)

[Title: "energy saving"] AND [Title: "machine learning"]

