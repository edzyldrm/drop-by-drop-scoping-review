---
title: "Drop by Drop Scoping Review"
author: "Birce Yilmazkarasu, Deniz Iren, Gjalt-Jorn Ygram Peters and Roland Klemke"
date: "`r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (UTC%z)')`"
output:
  pdf_document:
    citation_package: default
    pandoc_args: [ "--csl", "../apa.csl" ]
  html_document:
    toc: false
link-citations: yes
bibliography: "../bibliography.bib"
biblio-style: apalike
geometry: "paperheight=29.7cm,paperwidth=21cm,left=3cm,right=3cm,top=2cm,bottom=3cm"
editor_options: 
  chunk_output_type: console
---

# Drop by Drop scoping review {.tabset .tabset-pills}

```{r, setup, echo=FALSE, include=FALSE}

###-----------------------------------------------------------------------------
### Preparation
###-----------------------------------------------------------------------------

if (!("ufs" %in% installed.packages()[, 'Package'])) {
  install.packages('ufs');
  # remotes::install_gitlab("r-packages/ufs@dev");
}

ufs::checkPkgs("here");
ufs::checkPkgs("readxl");
ufs::checkPkgs("writexl");

```

## Analysis script

The analysis script is [available here](drop-by-drop-scoping-review-analyses.html).

## Manuscript

The manuscript appears below.

```{r, download-manuscript, echo=FALSE, include=FALSE}

###-----------------------------------------------------------------------------
### Download and locally store manuscript text
###-----------------------------------------------------------------------------

manuscriptURL <-
  paste0(
    "https://docs.google.com/document/d/",
    "1IaQH6oyF-RIKMHDxHkyqDPxwCcNvLPrfburlX1CGt_g",
    "/export?format=txt"
  );
localManuscriptFile <-
  here::here("yilmazkarasu--iren--peters--klemke---2023---drop-by-drop-scoping-review---manuscript.rmd");

tryCatch(
  {
    ### Try to download plain text from Google Docs
    con <- url(manuscriptURL,
               encoding = "UTF-8");
    manuscriptText_raw <-
      readLines(
        con = con,
        warn = FALSE
      );
    
    ### If we didn't encounter any errors, process it
    manuscriptText_raw <- tail(manuscriptText_raw, -1);
    manuscriptText <- iconv(manuscriptText_raw,
                            from="UTF-8", to="UTF-8");
    manuscriptText <- gsub("“", '"', manuscriptText);
    manuscriptText <- gsub("”", '"', manuscriptText);
    manuscriptText <- gsub("‘", "'", manuscriptText);
    manuscriptText <- gsub("’", "'", manuscriptText);
    manuscriptText <- gsub("[^A-Za-z0-9_=+,./!?&)(><\\\\]\\\\[@-]", '~~<!!>~~', manuscriptText);
    
    ### And save a local backup
    writeLines(
      manuscriptText,
      localManuscriptFile
    );
  },
  error = function(e) {
    
    ### If we did encounter an error, load the local backup
    con <- file(localManuscriptFile,
             encoding = "UTF-8");
    manuscriptText <-
      readLines(
        con = con,
        warn = FALSE
      );
    
  },
  finally = close(con)
);

###-----------------------------------------------------------------------------
### Download and save bibliography
###-----------------------------------------------------------------------------

reference_identifiers <-
  capture.output(
    ufs::zotero_download_and_export_items(
      4880245,
      file = here::here("bibliography.bib")
    )
  );

# call1 <- 
#   ufs::zotero_construct_export_call(2792807, start=0, limit=80);
# call2 <- 
#   ufs::zotero_construct_export_call(2792807, start=80, limit=80);
# 
# bibCon <- url(call1);
# bib_part1 <- readLines(bibCon, warn = FALSE);
# close(bibCon);
# 
# bibCon <- url(call2);
# bib_part2 <- readLines(bibCon, warn = FALSE);
# close(bibCon);
# 
# bibliography <- c(bib_part1, bib_part2);
# 
# writeLines(bibliography, here::here("bibliography.bib"));
# 
# reference_identifiers <-
#   gsub("^@[a-zA-Z0-9]+\\{(.*),", "\\1", 
#        bibliography)[grep("^@[a-zA-Z0-9]+\\{(.*),", 
#        bibliography)];
# 
# reference_identifiers <-
#   gsub("\\[\\d+\\] \\\"(.*)\".*", "\\1", reference_identifiers);

reference_identifiers <-
  sort(trimws(unique(reference_identifiers)));

```

```{r, eval=knitr::is_html_output(), echo=FALSE, results='asis'}
cat("<strong style='display:block;text-align:center'>The PDF with this article is available <a href='yilmazkarasu--iren--peters--klemke---2023---drop-by-drop-scoping-review---manuscript.pdf'>here</a>.</strong>");
```

```{r, child=localManuscriptFile}
```

```{r, eval=knitr::is_html_output(), echo=FALSE, results='asis'}
cat("\n\n# Reference identifiers (tool during writing)\n\n");
cat(paste0("- ", reference_identifiers), sep="\n\n");
```

# References
